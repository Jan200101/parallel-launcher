mkdir templang
cp lang/*.ts templang
cd templang

mv parallel-launcher.de.ts app.de.ts
mv parallel-launcher.es.ts app.es.ts
mv parallel-launcher.fr.ts app.fr.ts
mv parallel-launcher.fr_ca.ts app.fr_ca.ts
mv parallel-launcher.it.ts app.it.ts
mv parallel-launcher.ar.ts app.ar.ts
mv parallel-launcher.pt.ts app.pt.ts

curl https://raw.githubusercontent.com/qt/qttranslations/dev/translations/qtbase_de.ts -UseBasicParsing -OutFile qt.de.ts
curl https://raw.githubusercontent.com/qt/qttranslations/dev/translations/qtbase_es.ts -UseBasicParsing -OutFile qt.es.ts
curl https://raw.githubusercontent.com/qt/qttranslations/dev/translations/qtbase_fr.ts -UseBasicParsing -OutFile qt.fr.ts
curl https://raw.githubusercontent.com/qt/qttranslations/dev/translations/qtbase_it.ts -UseBasicParsing -OutFile qt.it.ts
curl https://raw.githubusercontent.com/qt/qttranslations/dev/translations/qtbase_ar.ts -UseBasicParsing -OutFile qt.ar.ts
curl https://raw.githubusercontent.com/qt/qttranslations/dev/translations/qtbase_pt_BR.ts -UseBasicParsing -OutFile qt.pt.ts

C:\Qt\5.15.2\msvc2019_64\bin\lconvert.exe -i app.de.ts qt.de.ts -o parallel-launcher.de.ts
C:\Qt\5.15.2\msvc2019_64\bin\lconvert.exe -i app.es.ts qt.es.ts -o parallel-launcher.es.ts
C:\Qt\5.15.2\msvc2019_64\bin\lconvert.exe -i app.fr.ts qt.fr.ts -o parallel-launcher.fr.ts
C:\Qt\5.15.2\msvc2019_64\bin\lconvert.exe -i app.fr_ca.ts qt.fr.ts -o parallel-launcher.fr_ca.ts
C:\Qt\5.15.2\msvc2019_64\bin\lconvert.exe -i app.it.ts qt.it.ts -o parallel-launcher.it.ts
C:\Qt\5.15.2\msvc2019_64\bin\lconvert.exe -i app.ar.ts qt.ar.ts -o parallel-launcher.ar.ts
C:\Qt\5.15.2\msvc2019_64\bin\lconvert.exe -i app.pt.ts qt.pt.ts -o parallel-launcher.pt.ts

C:\Qt\5.15.2\msvc2019_64\bin\lrelease.exe parallel-launcher.de.ts
C:\Qt\5.15.2\msvc2019_64\bin\lrelease.exe parallel-launcher.en_us.ts
C:\Qt\5.15.2\msvc2019_64\bin\lrelease.exe parallel-launcher.en_gb.ts
C:\Qt\5.15.2\msvc2019_64\bin\lrelease.exe parallel-launcher.es.ts
C:\Qt\5.15.2\msvc2019_64\bin\lrelease.exe parallel-launcher.fr.ts
C:\Qt\5.15.2\msvc2019_64\bin\lrelease.exe parallel-launcher.fr_ca.ts
C:\Qt\5.15.2\msvc2019_64\bin\lrelease.exe parallel-launcher.it.ts
C:\Qt\5.15.2\msvc2019_64\bin\lrelease.exe parallel-launcher.ar.ts
C:\Qt\5.15.2\msvc2019_64\bin\lrelease.exe parallel-launcher.pt.ts

rm *.ts
rm ..\release\translations\parallel-launcher.*.qm -ErrorAction SilentlyContinue
cp *.qm ..\release\translations
rm *.qm
cd ..
rm templang
