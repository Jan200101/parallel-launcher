Name:		parallel-launcher
Version:	GITLAB_CI_VERSION
Release:	0%{?dist}
Summary:	Modern N64 Emulator
License:	GPLv3
Group:		games
URL:		https://parallel-launcher.ca
Source:		https://gitlab.com/parallel-launcher/parallel-launcher/-/archive/GITLAB_CI_TAG/parallel-launcher-GITLAB_CI_TAG.tar.bz2
Vendor:		Matt Pharoah

BuildRequires: (gcc >= 9 or gcc9 or gcc10 or gcc11 or gcc12 or gcc-toolset-9-gcc or gcc-toolset-10-gcc or gcc-toolset-11-gcc or gcc-toolset-12-gcc)
BuildRequires: (gcc-c++ >= 9 or gcc9-c++ or gcc10-c++ or gcc11-c++ or gcc12-c++ or gcc-toolset-9-gcc-c++ or gcc-toolset-10-gcc-c++ or gcc-toolset-11-gcc-c++ or gcc-toolset-12-gcc-c++)
BuildRequires: ((libqt5-qtbase-common-devel and libQt5DBus-devel) or qt5-qtbase-devel)
BuildRequires: (libqt5-qtdeclarative-devel or qt5-qtdeclarative-devel)
BuildRequires: (libSDL2-devel or SDL2-devel)
BuildRequires: flatpak-devel
BuildRequires: (sqlite3-devel or sqlite-devel)
BuildRequires: libgcrypt-devel
BuildRequires: (qt5-linguist or libqt5-linguist)
BuildRequires: (qt5-qtsvg-devel or libqt5-qtsvg-devel)

Requires: glibc
Requires: (libstdc++6 or libstdc++)
Requires: (libgcc_s1 or libgcc)
Requires: ((libQt5Core5 and libQt5Widgets5 and libQt5Network5 and libQt5Svg5 and libQt5DBus5) or (qt5-qtbase and qt5-qtbase-common and qt5-qtdeclarative and qt5-qtsvg and (qt5-qdbus or qt5-qttools)))
Requires: (libQt5Gui5 or qt5-qtbase-gui)
Requires: findutils
Requires: xdg-utils
Requires: flatpak
Requires: polkit
Requires: (libsqlite3-0 or sqlite-libs)
Requires: (libgcrypt or libgcrypt20)
Requires: dosfstools
Requires: (coreutils or coreutils-common)
Requires: p7zip

Recommends: (google-noto-sans-fonts or noto-sans-fonts)
Recommends: (google-noto-sans-mono-fonts or noto-mono-fonts)
Recommends: udisks2

%define debug_package %{nil}

%description
A simple easy-to-use launcher for the ParallelN64 and Mupen64Plus-Next emulators.

%prep
%setup -q

%build
if [ `g++ -dumpversion` -lt 9 ]; then
	if [ `which g++-12` ]; then
		qmake-qt5 app.pro -spec linux-g++ QMAKE_CXX="g++-12"
	elif [ `which g++-11` ]; then
		qmake-qt5 app.pro -spec linux-g++ QMAKE_CXX="g++-11"
	elif [ `which g++-10` ]; then
		qmake-qt5 app.pro -spec linux-g++ QMAKE_CXX="g++-10"
	elif [ `which g++-9` ]; then
		qmake-qt5 app.pro -spec linux-g++ QMAKE_CXX="g++-9"
	else
		echo "WARNING: g++ is out-of-date. You may need to update."
		qmake-qt5 app.pro -spec linux-g++
	fi
else
	qmake-qt5 app.pro -spec linux-g++
fi
if [ `which lrelease-qt5` ]; then
	lrelease-qt5 app.pro
else
	lrelease app.pro
fi
make -j `nproc`

%install
install -D parallel-launcher %{buildroot}/usr/bin/parallel-launcher
install -D ca.parallel_launcher.ParallelLauncher.desktop %{buildroot}/usr/share/applications/ca.parallel_launcher.ParallelLauncher.desktop
install -D ca.parallel_launcher.ParallelLauncher.metainfo.xml %{buildroot}/usr/share/metainfo/ca.parallel_launcher.ParallelLauncher.metainfo.xml
install -D data/appicon.svg %{buildroot}/usr/share/icons/hicolor/scalable/apps/ca.parallel_launcher.ParallelLauncher.svg
install -D bps-mime.xml %{buildroot}/usr/share/parallel-launcher/bps-mime.xml
install -D lang/parallel-launcher.en_us.qm %{buildroot}/usr/share/parallel-launcher/parallel-launcher.en_us.qm
install -D lang/parallel-launcher.en_gb.qm %{buildroot}/usr/share/parallel-launcher/parallel-launcher.en_gb.qm
install -D lang/parallel-launcher.it.qm %{buildroot}/usr/share/parallel-launcher/parallel-launcher.it.qm
install -D lang/parallel-launcher.es.qm %{buildroot}/usr/share/parallel-launcher/parallel-launcher.es.qm
install -D lang/parallel-launcher.fr_ca.qm %{buildroot}/usr/share/parallel-launcher/parallel-launcher.fr_ca.qm
install -D lang/parallel-launcher.fr.qm %{buildroot}/usr/share/parallel-launcher/parallel-launcher.fr.qm
install -D lang/parallel-launcher.de.qm %{buildroot}/usr/share/parallel-launcher/parallel-launcher.de.qm
install -D lang/parallel-launcher.ar.qm %{buildroot}/usr/share/parallel-launcher/parallel-launcher.ar.qm
install -D lang/parallel-launcher.pt.qm %{buildroot}/usr/share/parallel-launcher/parallel-launcher.pt.qm
install -D parallel-launcher-sdl-proxy %{buildroot}/usr/share/parallel-launcher/parallel-launcher-sdl-proxy
install -D parallel-launcher-lsjs %{buildroot}/usr/share/parallel-launcher/parallel-launcher-lsjs

%files
/usr/bin/parallel-launcher
/usr/share/applications/ca.parallel_launcher.ParallelLauncher.desktop
/usr/share/metainfo/ca.parallel_launcher.ParallelLauncher.metainfo.xml
/usr/share/icons/hicolor/scalable/apps/ca.parallel_launcher.ParallelLauncher.svg
/usr/share/parallel-launcher/bps-mime.xml
/usr/share/parallel-launcher/parallel-launcher.en_us.qm
/usr/share/parallel-launcher/parallel-launcher.en_gb.qm
/usr/share/parallel-launcher/parallel-launcher.it.qm
/usr/share/parallel-launcher/parallel-launcher.es.qm
/usr/share/parallel-launcher/parallel-launcher.fr_ca.qm
/usr/share/parallel-launcher/parallel-launcher.fr.qm
/usr/share/parallel-launcher/parallel-launcher.de.qm
/usr/share/parallel-launcher/parallel-launcher.ar.qm
/usr/share/parallel-launcher/parallel-launcher.pt.qm
/usr/share/parallel-launcher/parallel-launcher-sdl-proxy
/usr/share/parallel-launcher/parallel-launcher-lsjs

%post
if [ $1 -eq 1 ] ; then
	xdg-mime install /usr/share/parallel-launcher/bps-mime.xml 2> /dev/null || true
	xdg-mime default ca.parallel_launcher.ParallelLauncher.desktop x-scheme-handler/rhdc
	update-desktop-database /usr/share/applications 2> /dev/null || true
fi
exit 0

%postun
if [ $1 -eq 0 ] ; then
	xdg-mime uninstall /usr/share/parallel-launcher/bps-mime.xml 2> /dev/null || true
	update-desktop-database /usr/share/applications 2> /dev/null || true
fi
if [ $1 -eq 1 ] ; then
	rm -f /etc/flatpak/installations.d/parallel-launcher.conf
fi
exit 0
