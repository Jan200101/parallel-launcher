#ifndef SRC_CORE_MIGRATION_HPP_
#define SRC_CORE_MIGRATION_HPP_

namespace Migration {
	extern void run();
	extern void updateDatabaseSchema();
}


#endif /* SRC_CORE_MIGRATION_HPP_ */
