#include "src/rhdc/web/api.hpp"
#include "src/rhdc/web/api-helpers.hpp"

#include <QCryptographicHash>
#include <QNetworkInterface>
#include <QCoreApplication>
#include <QDesktopWidget>
#include <QApplication>
#include <QSysInfo>
#include <QScreen>
#include <QLocale>
#include <QTimer>
#include <memory>
#include "src/polyfill/base-directory.hpp"
#include "src/db/data-provider.hpp"
#include "src/core/logging.hpp"

struct __telemetry_t {};

static inline int getInterfaceRelevance( const QNetworkInterface &interface ) {
	if( !interface.isValid() ) return -1;
	if( interface.flags().testFlag( QNetworkInterface::IsLoopBack ) ) return -1;

	switch( interface.type() ) {
		case QNetworkInterface::Ethernet:
		case QNetworkInterface::Wifi:
			return (interface.name() == "eth0") ? 5 : 4;
		case QNetworkInterface::Virtual:
			return 2;
		case QNetworkInterface::Unknown:
			return 1;
		case QNetworkInterface::Loopback:
			return -1;
		default:
			return 3;
	}
}

static inline QString getMacAddress() {
	QNetworkInterface eth0 = QNetworkInterface::interfaceFromName( "eth0" );
	if( eth0.isValid() && eth0.type() != QNetworkInterface::Loopback ) {
		return eth0.hardwareAddress();
	}

	QNetworkInterface bestMatch;
	int bestScore = 0;

	for( const QNetworkInterface &interface : QNetworkInterface::allInterfaces() ) {
		const int relevanceScore = getInterfaceRelevance( interface );
		if( relevanceScore > bestScore ) {
			bestMatch = interface;
			bestScore = relevanceScore;
		}
	}

	return bestMatch.isValid() ? bestMatch.hardwareAddress() : QString();
}

static string getUniqueIdentifier() {
	QString id = getMacAddress();
	if( id.isEmpty() ) {
#ifdef __linux__
		id = QSysInfo::machineUniqueId().toHex().toStdString().c_str();
#else
		id = QSysInfo::machineHostName();
#endif
	}

	const QByteArray hashedId = QCryptographicHash::hash( id.toUtf8(), QCryptographicHash::Sha3_256 );
	return hashedId.toHex().toStdString();
}

static inline QByteArray serializeTelemetryData() {
	const QSize resolution = QApplication::primaryScreen()->size();

	std::ostringstream stream;
	JsonWriter jw( &stream, false );

	jw.writeObjectStart();
	jw.writeProperty( "id", getUniqueIdentifier() );
	jw.writeProperty( "resolutionWidth", resolution.width() );
	jw.writeProperty( "resolutionHeight", resolution.height() );
	jw.writeProperty( "dpiX", QApplication::desktop()->logicalDpiX() );
	jw.writeProperty( "dpiY", QApplication::desktop()->logicalDpiY() );
	jw.writeProperty( "locale", QLocale().name().toStdString() );
	jw.writeProperty( "romCount", DataProvider::countRomFiles() );
	jw.writeProperty( "rhdcIntegrationEnabled", RhdcCredentials::exists() || DataProvider::hasRhdcData() );
	jw.writeObjectEnd();

	return QByteArray( stream.str().c_str() );
}

void RhdcApi::sendTelemetryDatum() {
	QNetworkReply *response = ApiUtil::send( HttpMethod::Post, RHDC_API_HOST "/pl/telemetry", "application/json", serializeTelemetryData(), false );
	ApiUtil::onResponse( response, [](){}, [](ApiErrorType){} );
}

static inline QByteArray getCrashLog( const fs::path filePath ) {
	if( !fs::existsSafe( filePath ) ) return QByteArray();

	QFile crashReport( filePath.u8string().c_str() );
	if( !crashReport.open( QIODevice::ReadOnly ) ) return QByteArray();

	const QByteArray report = crashReport.readAll();
	crashReport.close();

	if( !crashReport.remove() || fs::existsSafe( filePath ) ) {
		fs::error_code err;
		if( !fs::remove( filePath, err ) || fs::existsSafe( filePath ) ) {
			return QByteArray();
		}
	}

	return report;
}

void RhdcApi::sendCrashReport() {
	std::shared_ptr<bool> requestInFlight( new bool( true ) );

	QByteArray report = getCrashLog( BaseDir::data() / "crashlog.txt" );
#ifdef _WIN32
	if( report.isNull() ) report = getCrashLog( BaseDir::data() / "crashlog.2.txt" );
#endif
	if( report.isNull() || report.isEmpty() ) return;

	QNetworkReply *response = ApiUtil::send( HttpMethod::Post, RHDC_API_HOST "/pl/crash", "text/plain", report, false );
	ApiUtil::onResponse( response, [requestInFlight](){
		if( !*requestInFlight ) return;
		*requestInFlight = false;
	}, [requestInFlight](ApiErrorType){
		*requestInFlight = false;
	});

	QTimer::singleShot( 3000, [requestInFlight,response](){
		if( !*requestInFlight ) return;
		*requestInFlight = false;
		response->abort();
	});

	while( *requestInFlight ) {
		QCoreApplication::processEvents( QEventLoop::WaitForMoreEvents | QEventLoop::ExcludeUserInputEvents	);
	}
}
