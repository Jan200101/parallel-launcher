#ifndef SRC_RHDC_UI_RHDC_HACK_WIDGET_HPP_
#define SRC_RHDC_UI_RHDC_HACK_WIDGET_HPP_

#include <QWidget>
#include "src/rhdc/core/hack.hpp"

namespace Ui {
	class RhdcHackWidget;
}

class ThumbnailNotifier : public QObject {
	Q_OBJECT

	public:
	ThumbnailNotifier() : QObject( nullptr ) {}
	~ThumbnailNotifier() {}

	void notifyDownloaded();

	signals:
	void downloaded();
};

class RhdcHackWidget : public QWidget {
	Q_OBJECT

	private:
	Ui::RhdcHackWidget *m_ui;
	const RhdcHackExt m_hack;
	bool m_thumbnailLoaded;

	void loadThumbnail();

	protected:
	virtual void paintEvent( QPaintEvent *event ) override;

	public:
	RhdcHackWidget(
		QWidget *parent,
		const RhdcHackExt &hack,
		bool selected = false
	);
	~RhdcHackWidget();

	inline const RhdcHackExt &hack() const noexcept { return m_hack; }

	inline RhdcHackWidget *clone( QWidget *parent, bool selected ) const {
		return new RhdcHackWidget( parent, m_hack, selected );
	}

	public slots:
	void editorResized( int width );

};



#endif /* SRC_RHDC_UI_RHDC_HACK_WIDGET_HPP_ */
