#ifndef SRC_RHDC_UI_RHDC_RATING_DIALOG_HPP_
#define SRC_RHDC_UI_RHDC_RATING_DIALOG_HPP_

#include <QDialog>
#include "src/types.hpp"

namespace Ui {
	class RhdcRatingDialog;
}

class RhdcRatingDialog : public QDialog {
	Q_OBJECT

	private:
	Ui::RhdcRatingDialog *m_ui;
	bool m_kaizo;

	public:
	RhdcRatingDialog( QWidget *parent = nullptr );
	~RhdcRatingDialog();

	void setRatings( ubyte quality, ubyte difficulty, bool isKaizo );
	ubyte getQuality() const;
	ubyte getDifficulty() const;

	private slots:
	void qualityChanged( int rating );
	void difficultyChanged( int rating );

};



#endif /* SRC_RHDC_UI_RHDC_RATING_DIALOG_HPP_ */
