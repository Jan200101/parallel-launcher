#include "src/rhdc/ui/rhdc-save-editor.hpp"

#include <QCoreApplication>
#include <QDialogButtonBox>
#include <QVBoxLayout>
#include <QPushButton>
#include <QDialog>
#include "src/rhdc/ui/star-display-widget.hpp"
#include "src/ui/icons.hpp"
#include "src/ui/util.hpp"

void RhdcSaveEditor::exec(
	const fs::path &saveFilePath,
	const string &hackId
) {
	StarLayout layout;
	if( !StarLayout::tryLoadLayout( hackId, layout ) ) {
		return;
	}

	QDialog dialog;
	dialog.setModal( true );
	dialog.setSizeGripEnabled( true );
	dialog.setWindowTitle( QCoreApplication::translate( "RhdcSaveEditor", "Save Editor" ) );
	dialog.setWindowIcon( Icon::appIcon() );
	UiUtil::fixDialogButtonsOnWindows( &dialog );

	QVBoxLayout *widgets = new QVBoxLayout( &dialog );
	dialog.setLayout( widgets );

	EditableStarDisplayWidget *editor = new EditableStarDisplayWidget( &dialog, saveFilePath, std::move( layout ) );
	QDialogButtonBox *buttons = new QDialogButtonBox( QDialogButtonBox::Save | QDialogButtonBox::Discard, &dialog );
	widgets->addWidget( editor, 0 );
	widgets->addStretch( 1 );
	widgets->addWidget( buttons, 0 );

	QObject::connect( buttons->button( QDialogButtonBox::Discard ), &QPushButton::clicked, &dialog, &QDialog::reject );
	QObject::connect( buttons->button( QDialogButtonBox::Save ), &QPushButton::clicked, &dialog, [&](){
		editor->save();
		dialog.accept();
	});

	dialog.exec();
}
