#include "src/ui/screen.hpp"

#include <QGuiApplication>
#include <QApplication>
#include <QWindow>
#include <QWidget>
#include <QScreen>

static inline const QScreen *getActiveScreen() {
	const QWidget *const activeWindow = QApplication::activeWindow();
	if( activeWindow != nullptr ) {
		const QWindow *const handle = activeWindow->windowHandle();
		if( handle != nullptr ) {
			const QScreen *screen = handle->screen();
			if( screen != nullptr ) return screen;
		}
	}

	for( const QWidget *window : QApplication::topLevelWidgets() ) {
		const QWindow *const handle = window->windowHandle();
		if( handle == nullptr ) continue;

		const QScreen *screen = handle->screen();
		if( screen != nullptr ) return screen;
	}

	return QGuiApplication::primaryScreen();
}

void Screen::getResolution( int &width, int &height ) {
	const QSize resolution = getActiveScreen()->size();
	width = resolution.width();
	height = resolution.height();
}

void Screen::getDesktopSize( int &width, int &height ) {
	const QSize resolution = getActiveScreen()->availableSize();
	width = resolution.width();
	height = resolution.height();
}
